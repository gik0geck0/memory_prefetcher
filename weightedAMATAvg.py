#!/usr/bin/env python3

import sys

if len(sys.argv) > 5:
    cumulativeSpeedup = float(sys.argv[1])
    sectionTime = float(sys.argv[2])
    totalTime = float(sys.argv[3])
    baseAMAT = float(sys.argv[4])
    optAMAT = float(sys.argv[5])
    cumulativeSpeedup += sectionTime / totalTime * baseAMAT / optAMAT
    print("%0.8lf" % (cumulativeSpeedup))
