#!/bin/bash


pushd bin
execs=$(ls . | grep -v example | grep -v noprefetching)
popd

outputAwkCompare='/AMAT/ {print $2}'
awkAverageAMAT='BEGIN { avg=0.0; sum=0.0; count=0; } /AMAT/ { sum+=$2; count+=1; avg=sum/count;} END { print avg; }'

# Explicitly run the example first
for trace in $(find ./traces -name '*.trace'); do
    echo "$trace".example.out
    ./bin/example "$trace"
    mv "$trace".out "$trace".example.out
done
exampleAverage=$(cat traces/*.trace.example.out | awk "$awkAverageAMAT")

for trace in $(find ./traces -name '*.trace'); do
    echo "$trace".noprefetching.out
    ./bin/noprefetching "$trace"
    mv "$trace".out "$trace".noprefetching.out
done
noPrefetchAverage=$(cat traces/*.trace.noprefetching.out | awk "$awkAverageAMAT")

for trace in $(find ./traces -name '*.trace'); do
    echo -e "\n$trace"
    exampleTime=$(awk "$outputAwkCompare" "$trace".example.out)
    noprefetchingTime=$(awk "$outputAwkCompare" "$trace".noprefetching.out)
    # echo $trace".example AMAT = $exampleTime"
    # echo $trace".noprefetching AMAT = $exampleTime"
    for exec in ${execs}; do
        # echo "$trace"."$exec".out
        # TODO: The history size is hard-coded here in the script, but only fancy striding uses it
        ./bin/"$exec" "$trace" 10
        mv "$trace".out "$trace"."$exec".out

        # Compare this algo vs example, and print stat differences
        thisTime=$(awk "$outputAwkCompare" "$trace"."$exec".out)

        echo "$exec ($thisTime) vs example ($exampleTime): $(./speedup.py $exampleTime $thisTime)"
        echo "$exec ($thisTime) vs noprefetching ($noprefetchingTime): $(./speedup.py $noprefetchingTime $thisTime)"
    done
done

if [ "$1" == "avg" ]; then
    oneRuntime='/total run time/ { print $4; }'
    awkAvgRuntime='BEGIN { avg=0.0; sum=0.0; count=0; } /total run time/ { sum+=$4; count+=1; avg=sum/count;} END { print avg; }'

    # Avg 1: speedup(sum Runtime / traces)
    echo -e "\n"
    exampleAvgRuntime=$(cat traces/*.trace.example.out | awk "$awkAvgRuntime")
    noPrefetchAvgRuntime=$(cat traces/*.trace.noprefetching.out | awk "$awkAvgRuntime")
    for exec in ${execs}; do
        thisAvg=$(cat traces/*.trace."$exec".out | awk "$awkAvgRuntime")
        echo -e "$exec global speedup over example: $(./speedup.py $exampleAvgRuntime $thisAvg)"
        echo -e "$exec global speedup over noprefetching: $(./speedup.py $noPrefetchAvgRuntime $thisAvg)"
    done

    # Avg 2: avg(speedup(trace))
    for exec in ${execs}; do
        exSSum=0
        nopreSSum=0
        traces=0
        for trace in $(find ./traces -name '*.trace'); do
            thisTime=$(awk $oneRuntime "$trace"."$exec".out)
            exSpeedup=$(./speedup $(awk $oneRuntime "$trace".example.out) $thisTime)
            nopreSpeedup=$(./speedup $(awk $oneRuntime "$trace".noprefetching.out) $thisTime)
            exSSum=$(echo "$exSSum + $exSpeedup" | bc)
            nopreSSum=$(echo "$nopreSSum + $nopreSpeedup" | bc)
            traces=$(echo "$traces + 1" | bc)
        done
    done
fi
