
awkAverageAMAT='BEGIN { avg=0.0; sum=0.0; count=0; } /AMAT:/ { sum+=$2; count+=1; avg=sum/count;} END { print avg; }'


noPrefetchAverage=$(cat traces/{grep,g++,ls,plamap,testgen}.trace.noprefetching.out | awk "$awkAverageAMAT")
# echo "No Prefetching average: $noPrefetchAverage"

# Compile the core/utility parts
g++ -c cache.cpp -o cache.o
g++ -c CPU.cpp -o CPU.o
g++ -c memQueue.cpp -o memQueue.o

rangeMax="0 0"
bestAvg="0.0"

for linerange in $(seq $1 $2); do
    for lines in $(seq $3 2 $4); do
        # compile the program with these parameters
        g++ -DLINESRANGE=$linerange -DNLINES=$lines -DRANDNEXT main.cpp prefetcher_randNext.cpp cache.o CPU.o memQueue.o -o bin/randNext_"$linerange"_"$lines" &
        nextLine=$(($lines+1))
        g++ -DLINESRANGE=$linerange -DNLINES=$nextLine -DRANDNEXT main.cpp prefetcher_randNext.cpp cache.o CPU.o memQueue.o -o bin/randNext_"$linerange"_"$nextLine"

        # run it, and find average AMAT across all 5 inputs
        for trace in $(find ./traces -name '*.trace'); do
            ./bin/randNext_"$linerange"_"$lines" "$trace"
            mv "$trace".out "$trace".randNext_"$linerange"_"$lines".out

            ./bin/randNext_"$linerange"_"$nextLine" "$trace"
            mv "$trace".out "$trace".randNext_"$linerange"_"$nextLine".out
        done


        thisAverage=$(cat traces/{grep,g++,ls,plamap,testgen}.trace.randNext_"$linerange"_"$lines".out | awk "$awkAverageAMAT")
        thisSpeedup="$(./speedup.py $noPrefetchAverage $thisAverage 100)"
        # echo -e "[$linerange, $lines]\t$thisSpeedup"

        nextAverage=$(cat traces/{grep,g++,ls,plamap,testgen}.trace.randNext_"$linerange"_"$nextLine".out | awk "$awkAverageAMAT")
        nextSpeedup="$(./speedup.py $noPrefetchAverage $nextAverage 100)"
        # echo -e "[$linerange, $nextLine]\t$nextSpeedup"

        if [ "$(./floatcmp.py $bestAvg $thisSpeedup)" -lt "0" ]; then
            rangeMax="$linerange $lines"
            bestAvg="$thisSpeedup"
        fi

        if [ "$(./floatcmp.py $bestAvg $nextSpeedup)" -lt "0" ]; then
            rangeMax="$linerange $nextLine"
            bestAvg="$nextSpeedup"
        fi

        # Cleanup the outputs. There will be a LOT of files
        rm traces/{grep,g++,ls,plamap,testgen}.trace.randNext_"$linerange"_"$lines".out
        rm bin/randNext_"$linerange"_"$lines"

        rm traces/{grep,g++,ls,plamap,testgen}.trace.randNext_"$linerange"_"$nextLine".out
        rm bin/randNext_"$linerange"_"$nextLine"
    done
done
rm cache.o CPU.o memQueue.o

echo -e "Best was $rangeMax at $bestAvg"
