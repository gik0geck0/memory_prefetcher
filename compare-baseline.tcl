#! /usr/bin/env tclsh8.6

proc extract-number {str} {
  string trim [lindex [split $str :] end]
}

proc compare {new baseline} {
  set ratio [expr {100.0 * [extract-number $new] / [extract-number $baseline]}]
  format "x %3.2f: %s" $ratio $new
}

set baseline [open [lindex $argv 0] r]
set GRADING_BASELINE_RUNTIME 11.354420999999999
set weighted_speedup 0.0

while {![eof stdin]} {
  set in [gets stdin]
  set bl [gets $baseline]
  if {$in ne {}} {
    puts [compare $in $bl]
  }

  if {-1 != [string first "AMAT" $in]} {
    set speedup [expr {[extract-number $bl]/double([extract-number $in])}]
    puts "AMAT speedup: $speedup"
    set weighted_speedup [expr {
        $weighted_speedup +
        $speedup * [extract-number $bl] / $GRADING_BASELINE_RUNTIME}]
  }

  if {-1 != [string first "Memory BW" $in]} {
    puts ""
  }
}

close $baseline

puts "Weighted speed-up: $weighted_speedup"
