#include "prefetcher_randNext.h"
#include "mem-sim.h"
#define LINESRANGE 6
#define NLINES 7

Prefetcher::Prefetcher(int,char** argv) { srand(66); }

bool Prefetcher::hasRequest(uint32_t cycle) {
    return !queuedRequests.empty();
}

Request Prefetcher::getRequest(uint32_t cycle) {
    uint32_t nextAddr = queuedRequests.front();
    queuedRequests.pop_front();
    Request nextReq;
    nextReq.addr = nextAddr;
    // printf("Requesting %u\n", nextAddr);
    return nextReq;
}

void Prefetcher::completeRequest(uint32_t cycle) {
}

void Prefetcher::cpuRequest(Request req) {
    // Once we get a miss, we have the opportunity to request.
    // Fetch N items within 1024 lines in either direction of the miss
    const static int linesRange = 10;
    if (!req.HitL1) {
        queuedRequests.push_back(req.addr % 224 + req.addr);

        for (int i=0; i < NLINES; ++i) {
            int addr = ((double) rand() / RAND_MAX) * 2 * LINESRANGE;    // Uniform [-LINESRANGE, LINESRANGE]
            addr *= 16;
            queuedRequests.push_back(req.addr + addr);
        }
    }

    last = req.addr;
}
