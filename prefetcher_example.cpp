#include "prefetcher_example.h"
#include "mem-sim.h"

Prefetcher::Prefetcher(int,char**) { _ready = false; }

bool Prefetcher::hasRequest(uint32_t cycle) {
    return _ready;
}

Request Prefetcher::getRequest(uint32_t cycle) { return _nextReq; }

void Prefetcher::completeRequest(uint32_t cycle) { _ready = false; }

void Prefetcher::cpuRequest(Request req) {
	if(!_ready && !req.HitL1) {
		_nextReq.addr = req.addr + 16;
		_ready = true;
	}
}
