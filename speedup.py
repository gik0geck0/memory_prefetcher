#!/usr/bin/env python3

import sys

if len(sys.argv) >= 3:
    arg1 = float(sys.argv[1])
    arg2 = float(sys.argv[2])
    speedup = arg1 / arg2
    mul = 1
    if len(sys.argv) == 4:
        mul = float(sys.argv[3])
    print("%0.8lf" % (speedup*mul))
