CXX = g++
CXXFLAGS+= -g -std=c++11
# CXXFLAGS+= -DDEBUG

PREFETCHERS= example fancyBlockStriding noprefetching randNext submission
PREFETCHERS_SRC=$(addprefix prefetcher_,$(PREFETCHERS))
CORESRC= CPU cache main memQueue
COREDIR=$(addprefix object_files/, $(CORESRC))
COREOBJ=$(addsuffix .o, $(COREDIR))
CORECPP=$(addsuffix .cpp, $(CORESRC))

capitalize=$(shell echo $1 | tr a-z A-Z)

all: $(PREFETCHERS)

$(PREFETCHERS): % : prefetcher_%.cpp $(CORECPP)
	@mkdir -p bin
	${CXX} ${CXXFLAGS} -D$(call capitalize,$(@)) -o bin/$(@) $(^)

# object_files/%.o: %.cpp
# 	@mkdir -p  $(@D)
# 	${CXX} ${CXXFLAGS} -o $(@) -c $(<)

clean:
	rm -f object_files/*.o bin/*
