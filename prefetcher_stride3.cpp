#include "prefetcher_stride3.h"
#include "mem-sim.h"

Prefetcher::Prefetcher(int,char**) {
    _ready = false;
    tribuf = (uint32_t*) calloc(3, sizeof(uint32_t));
}

bool Prefetcher::hasRequest(uint32_t cycle) { return _ready; }

Request Prefetcher::getRequest(uint32_t cycle) { return _nextReq; }

void Prefetcher::completeRequest(uint32_t cycle) { _ready = false; }

void Prefetcher::cpuRequest(Request req) {
    // This implementation mandates that the access are after one another
    // Detect a stride of 3, and queue up the 4th
    tribuf[2] = req.addr;
    if (fabs(tribuf[1]-tribuf[0]) == fabs(tribuf[2]-tribuf[1])) {
        // 3 sequential addresses with even spacing. Fetch one more space forward
        _nextReq.addr = tribuf[2] + (tribuf[2] - tribuf[1]);
        _ready = true;
        // yes, this works for decreasing strides
    }

    // Rotate tribuf
    tribuf[0] = tribuf[1];
    tribuf[1] = tribuf[2];
}
