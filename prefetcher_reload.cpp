#include "prefetcher_reload.h"

Prefetcher::Prefetcher(int,char**) {
    strideCounter = 0;
    tribuf = (uint32_t*) calloc(3, sizeof(uint32_t));
}

bool Prefetcher::hasRequest(uint32_t cycle) {
    updateLines();
    return !queuedRequests.empty();
}

Request Prefetcher::getRequest(uint32_t cycle) {
    uint32_t nextAddr = queuedRequests.front();
    queuedRequests.pop_front();
    Request nextReq;
    nextReq.addr = nextAddr;
    // printf("Requesting %u\n", nextAddr);
    return nextReq;
}

void Prefetcher::completeRequest(uint32_t cycle) {
    // Uhhh... do nothing?
}

void Prefetcher::cpuRequest(Request req) {
    // This implementation mandates that the access are after one another
    // Detect a stride of 3, and queue up the 4th
    tribuf[2] = req.addr;

    if (queuedRequests.size() < 1024 && fabs(tribuf[1] - tribuf[0]) == fabs(tribuf[2]-tribuf[1]) && (tribuf[2]+4)/4 != tribuf[2]/4) {
        // printf("Stride detected: %u -> %u -> %u (%u,%u,%u)\n", tribuf[0], tribuf[1], tribuf[2], tribuf[0]/4, tribuf[1]/4, tribuf[2]/4);
        if (req.HitL1) {
            strideCounter++; // fabs(tribuf[2]-tribuf[1]);
            // printf("Stride -> %u\n", strideCounter);
            // queuedRequests.clear();
        } else {
            if (tribuf[2] < tribuf[1]) {
                for (int i=0; i < strideCounter; ++i) {
                    uint32_t nextAddr = -4*i + tribuf[2];
                    if (nextAddr > 0) {
                        // printf("Adding %uth request (%u) until %u\n", i, nextAddr, strideCounter);
                        queuedRequests.push_back(nextAddr);
                    }
                }
            } else {
                for (int i=0; i < strideCounter; ++i) {
                    uint32_t nextAddr = 4*i + tribuf[2];
                    if (nextAddr > 0) {
                        // printf("Adding %uth request (%u) until %u\n", i, nextAddr, strideCounter);
                        queuedRequests.push_back(nextAddr);
                    }
                }
            }
            strideCounter = 0;
        }
        // 3 sequential addresses with even spacing. Fetch one more space forward
        // yes, this works for decreasing strides
    } else {
        if(!req.HitL1) {
            queuedRequests.push_back(req.addr + (req.addr % 583));
        }
    }
    /*
    if (!req.HitL1) {
        bool inQueue = false;
        for (auto i = queuedRequests.begin(); i != queuedRequests.end(); i++) {
            // printf("Checking %u against %u\n", *i, tribuf[2]);
            if (*i == tribuf[2]) {
                inQueue = true;
                break;
            }
        }

        if (!inQueue) {
            queuedRequests.clear();
            strideCounter = 0;
            queuedRequests.push_back(tribuf[2]+4);
        }
    }
    */

    // Rotate tribuf
    tribuf[0] = tribuf[1];
    tribuf[1] = tribuf[2];
    addLine(req.addr, req.HitL1);
}

void Prefetcher::addLine(uint32_t addr, bool isHit) {
    bool found = false;
    uint32_t addrNoIndex = addr & 0xfffffff0;
    // see if the address is currently in our queued lines
    for(int i = 0; i < mostUsed.size(); i++) {
        std::tuple<uint32_t, uint32_t, uint32_t> temp = mostUsed[i];
        if(std::get<1>(temp) == addrNoIndex) {
            // if yes, increment the bucket if the bucket isn't too full
            if(std::get<0>(temp) < 10000) { //10000, max bucket size
                std::get<0>(temp) += 100;  //increase the bucket by 100, maybe worth bigger numbers?
            }
            std::get<2>(temp) = 0;
            found = true;
        } else {
            uint32_t addr1;
            addr1 = std::get<1>(temp);
            if( (addrNoIndex & 0x00003ff0) == (addr1 & 0x00003ff0) && !isHit) {
                std::get<2>(temp)++;
                if(std::get<2>(temp) > 1 && std::get<0>(temp) > 50) {  //NOTE! change this to see how often something has to be rerequested before it gets loaded, tried 50, 100, 1000, 5000
                    //printf("re-requesting\n");
                    queuedRequests.push_back(addr1);
                }
            }
        }
        mostUsed[i] = temp;
    }
    if(!found) {
        std::tuple<uint32_t, uint32_t, uint32_t> temp(100, addrNoIndex, 0);
        mostUsed.push_back(temp);
    }
    std::sort(mostUsed.begin(), mostUsed.end());
    //if(mostUsed.size() > 500) {  //limit the max number of items in the queue, didn't seem to affect anything
    //    mostUsed.erase(mostUsed.begin() + 500, mostUsed.end());
    //}
}

void Prefetcher::updateLines() {
    for(int i = 0; i < mostUsed.size(); i++) {
        std::tuple<uint32_t, uint32_t, uint32_t> temp = mostUsed[i];
        std::get<0>(temp)--;
        if(std::get<0>(temp) > 0) {
            mostUsed[i] = temp;
        } else {
            mostUsed.erase(mostUsed.begin() + i);
            i--;
        }
    }
}


