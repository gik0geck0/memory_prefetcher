#include "prefetcher_fancyBlockStriding.h"

#define LINESRANGE 6
#define NLINES 7
#define HISTORYSIZE 49

Prefetcher::Prefetcher(int argc, char** argv):historySize(HISTORYSIZE), historyBuffer(HISTORYSIZE), strideJump(10) {
    srand(atoi(argv[2]));
}

bool Prefetcher::hasRequest(uint32_t cycle) {
    updateLines();
    return !queuedRequests.empty();
}

Request Prefetcher::getRequest(uint32_t cycle) {
    uint32_t nextAddr = queuedRequests.front();
    queuedRequests.erase(queuedRequests.begin());
    Request nextReq;
    nextReq.addr = nextAddr;
    // printf("Requesting %u\n", nextAddr);
    return nextReq;
}

void Prefetcher::completeRequest(uint32_t cycle) {
    // Uhhh... do nothing?
}

int debug(const char* format, ...) {
    int rv = 0;
    #if DEBUG
        va_list args;
        va_start(args, format);
        rv = vprintf(format, args);
        va_end(args);
    #endif
    return rv;
}

std::vector< std::vector<uint32_t> > detectAllStrides(std::vector<uint32_t> history) {
    std::vector< std::vector<uint32_t> > detected;
    // Stride jump refers to the gap between memory acceses (not the memory striding)
    for (uint32_t stride_start=0; stride_start < history.size()-2; ++stride_start) {
        for (uint32_t stride_next=stride_start+1; stride_next < history.size()-1; ++stride_next) {
            std::vector<uint32_t> thisStride;
            thisStride.push_back(history[stride_start]);
            thisStride.push_back(history[stride_next]);
            for (uint32_t stride_adv=stride_next+1; stride_adv < history.size(); ++stride_adv) {
                if (history[stride_adv] == thisStride.back()+thisStride[1]-thisStride[0]) {
                    thisStride.push_back(history[stride_adv]);
                }
            }
            if (thisStride.size() > 2)
                detected.push_back(thisStride);
        }
    }
    /* Regular stride access frequency
    for (uint32_t stridejump=1; stridejump < history.size()/2-1; ++stridejump) {
        for (uint32_t stridestart=0; stridestart < stridejump; ++stridestart) {
            std::vector<uint32_t> thisStride;
            uint32_t stridelen = 0;
            while (stridelen==0) {
                stridelen = *(history.begin()+stridestart+stridejump) - *(history.begin()+stridestart);
                if (stridelen==0 && stridestart+3*stridejump <= history.size()) {
                    stridestart+=stridejump;
                } else if (stridestart+3*stridejump > history.size()) {
                    goto next_start;
                }
            }

            thisStride.push_back(*(history.begin()+(int)stridestart));
            thisStride.push_back(*(history.begin()+stridestart+stridejump));
            for (uint32_t inter=stridestart+2*stridejump; inter < history.size(); inter+=stridejump) {
                if (*(history.begin()+inter) - *(history.begin()+inter-stridejump) == stridelen) {
                    thisStride.push_back(*(history.begin()+inter));
                }
            }
            if (thisStride.size() > 2) {
                detected.push_back(thisStride);
            }
            next_start: true;
        }
    }
    */
    return detected;
}

void Prefetcher::cpuRequest(Request req) {
    // This implementation mandates that the access are after one another
    // Detect a stride of 3, and queue up the 4th

    if (historyBuffer.size() >= historySize)
        historyBuffer.erase(historyBuffer.begin());
    historyBuffer.push_back(req.addr);


    if (!req.HitL1) {
        queuedRequests.clear();
        // printf("Detected hit!\n");
        // On miss, detect all strides
        std::vector< std::vector<uint32_t> > allStrides = detectAllStrides(historyBuffer);
        if (allStrides.size() > 0) {
            for (int i=0; i < allStrides.size(); ++i) {
                // Advance the stride by the number of elements in the stride
                for (int j=strideJump; j < strideJump+allStrides[i].size(); ++j) {
                    queuedRequests.push_back(allStrides[i].back()+j*(allStrides[i][1]-allStrides[i][0]));
                }

                // Advance the stride by 1
                // queuedRequests.push_back(*(allStrides[i].end()-1)+(allStrides[i][1]-allStrides[i][0]));
            }
        }

        // if (queuedRequests.empty()) {
            queuedRequests.push_back(req.addr % 224 + req.addr);

            for (int i=0; i < NLINES; ++i) {
                int addr = ((double) rand() / RAND_MAX) * 2 * LINESRANGE;    // Uniform [-LINESRANGE, LINESRANGE]
                addr *= 16;
                queuedRequests.push_back(req.addr + addr);
            }
        // next 4
        queuedRequests.push_back(req.addr + 16);
        queuedRequests.push_back(req.addr + 32);
        queuedRequests.push_back(req.addr + 48);
        queuedRequests.push_back(req.addr + 64);
        // }
    }
    addLine(req.addr, req.HitL1);
}

void Prefetcher::addLine(uint32_t addr, bool isHit) {
    bool found = false;
    uint32_t addrNoIndex = addr & 0xfffffff0;
    // see if the address is currently in our queued lines
    for(int i = 0; i < mostUsed.size(); i++) {
        std::tuple<uint32_t, uint32_t, uint32_t> temp = mostUsed[i];
        if(std::get<1>(temp) == addrNoIndex) {
            // if yes, increment the bucket if the bucket isn't too full
            if(std::get<0>(temp) < 10000) { //10000, max bucket size
                std::get<0>(temp) += 100;  //increase the bucket by 100, maybe worth bigger numbers?
            }
            std::get<2>(temp) = 0;
            found = true;
        } else {
            uint32_t addr1;
            addr1 = std::get<1>(temp);
            if( (addrNoIndex & 0x00003ff0) == (addr1 & 0x00003ff0) && !isHit) {
                std::get<2>(temp)++;
                if(std::get<2>(temp) > 1 && std::get<0>(temp) > 50) {  //NOTE! change this to see how often something has to be rerequested before it gets loaded, tried 50, 100, 1000, 5000
                    //printf("re-requesting\n");
                    queuedRequests.push_back(addr1);
                }
            }
        }
        mostUsed[i] = temp;
    }
    if(!found) {
        std::tuple<uint32_t, uint32_t, uint32_t> temp(100, addrNoIndex, 0);
        mostUsed.push_back(temp);
    }
    std::sort(mostUsed.begin(), mostUsed.end());
    //if(mostUsed.size() > 500) {  //limit the max number of items in the queue, didn't seem to affect anything
    //    mostUsed.erase(mostUsed.begin() + 500, mostUsed.end());
    //}
}

void Prefetcher::updateLines() {
    for(int i = 0; i < mostUsed.size(); i++) {
        std::tuple<uint32_t, uint32_t, uint32_t> temp = mostUsed[i];
        std::get<0>(temp)--;
        if(std::get<0>(temp) > 0) {
            mostUsed[i] = temp;
        } else {
            mostUsed.erase(mostUsed.begin() + i);
            i--;
        }
    }
}
