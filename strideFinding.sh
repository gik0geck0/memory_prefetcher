
outputAwkCompare='/AMAT/ {print $2}'
awkCountCycle='BEGIN { sum=0; } /total run time:/ { sum+=$4; } END { print sum; }'
getCycles='/total run time:/ {print $4}'

noPrefetchCycle=$(cat traces/{grep,g++,ls,plamap,testgen}.trace.noprefetching.out | awk "$awkCountCycle")

rangeMax="0 0 0"
bestAvg="0.0"
# for buffersize in $(seq 47 51); do
#     for strideJump in $(seq 2 5 50); do
#         for strideLen in $(seq 1 10); do
            for seed in $(seq 0 100); do
                # run it, and find average AMAT across all 5 inputs
                echo "" > ./traces/"$exec".out
                for trace in $(find ./traces -name '*.trace'); do
                    ./bin/randNext "$trace" $seed
                    cat "$trace".out >> ./traces/"$exec".out
                done

                thisSpeedup=$(cat ./traces/"$exec".out | ./compare-baseline.tcl ./traces/noprefetching.out | awk '/Weighted speed-up/ { print $3; }')
                echo "$seed: $thisSpeedup"

                # thisCycle=$(cat traces/*.trace."$exec".out | awk "$awkCountCycle")
                # thisSpeedup=$(./speedup.py $noPrefetchCycle $thisCycle)
                # echo -e "$exec total cycles\t$thisCycle, weighted speedup: $thisSpeedup, unweighted: $(./speedup.py $noPrefetchCycle $thisCycle)"

                # thisAverage=$(cat traces/{grep,g++,ls,plamap,testgen}.trace.fancyBlockStriding.out | awk "$awkAverageAMAT")
                # thisSpeedup="$(./speedup.py $noPrefetchAverage $thisAverage)"
                # echo -e "($noPrefetchAverage / $thisAverage)\t$thisSpeedup"

                if [ "$(./floatcmp.py $bestAvg $thisSpeedup)" -lt "0" ]; then
                    rangeMax="$seed"
                    bestAvg="$thisSpeedup"
                fi

                # rm traces/{grep,g++,ls,plamap,testgen}.trace.fancyBlockStriding.out
            done
#         done
#     done
# done

echo -e "Best was $rangeMax at $bestAvg"
