#ifndef PREFETCHER_H
#define PREFETCHER_H

#include <algorithm>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <vector>
#include <utility>
#include <tuple>

#include "mem-sim.h"

struct Request;

class Prefetcher {
public:
    Prefetcher(int argc, char** argv);
	// should return true if a request is ready for this cycle
	bool hasRequest(uint32_t cycle);

	// request a desired address be brought in
	Request getRequest(uint32_t cycle);

	// this function is called whenever the last prefetcher request was successfully sent to the L2
	void completeRequest(uint32_t cycle);

	/*
	 * This function is called whenever the CPU references memory.
	 * Note that only the addr, pc, load, issuedAt, and HitL1 should be considered valid data
	 */
	void cpuRequest(Request req);

private:
    uint32_t historySize;
    uint32_t strideJump;
    // uint32_t strideLen;
    std::vector<uint32_t> queuedRequests;
    std::vector<uint32_t> historyBuffer;
    std::vector<std::tuple<uint32_t, uint32_t, uint32_t> > mostUsed;

    void updateLines();
    void addLine(uint32_t, bool);
};

#endif
