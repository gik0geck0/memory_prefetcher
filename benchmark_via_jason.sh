#!/bin/bash

# Generate all output
allexecs=$(ls bin)
for exec in ${allexecs}; do
    echo "" > ./traces/"$exec".out
    for trace in $(find ./traces -name '*.trace'); do
        echo "./bin/"$exec" $trace 10"
        ./bin/"$exec" $trace 10
        cat "$trace".out >> ./traces/"$exec".out
    done
done

# compare everything against no prefetching
pushd bin
execs=$(ls . | grep -v example | grep -v noprefetching)
popd

for exec in ${execs}; do
    echo -n "$exec vs noprefetching: "
    cat ./traces/"$exec".out | ./compare-baseline.tcl ./traces/noprefetching.out | grep "Weighted speed-up"
done
