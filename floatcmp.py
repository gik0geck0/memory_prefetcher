#!/usr/bin/env python3
import sys

if len(sys.argv) == 3:
    arg1 = float(sys.argv[1])
    arg2 = float(sys.argv[2])
    if arg1 < arg2:
        print("-1")
    elif arg1 == arg2:
        print("0")
    else:
        print("1")
