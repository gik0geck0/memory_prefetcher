#!/bin/bash


pushd bin
execs=$(ls . | grep -v example | grep -v noprefetching)
popd

outputAwkCompare='/AMAT/ {print $2}'
awkCountCycle='BEGIN { sum=0; } /total run time:/ { sum+=$4; } END { print sum; }'
getCycles='/total run time:/ {print $4}'

# Explicitly run the example first
for trace in $(find ./traces -name '*.trace'); do
    echo "$trace".example.out
    ./bin/example "$trace"
    mv "$trace".out "$trace".example.out
done
exampleCycle=$(cat traces/*.trace.example.out | awk "$awkCountCycle")

for trace in $(find ./traces -name '*.trace'); do
    echo "$trace".noprefetching.out
    ./bin/noprefetching "$trace"
    mv "$trace".out "$trace".noprefetching.out
done
noPrefetchCycle=$(cat traces/*.trace.noprefetching.out | awk "$awkCountCycle")

for trace in $(find ./traces -name '*.trace'); do
    echo -e "\n$trace"
    exampleTime=$(awk "$outputAwkCompare" "$trace".example.out)
    noprefetchingTime=$(awk "$outputAwkCompare" "$trace".noprefetching.out)
    for exec in ${execs}; do
        # echo "$trace"."$exec".out
        ./bin/"$exec" "$trace" 49
        mv "$trace".out "$trace"."$exec".out

        # Compare this algo vs example, and print stat differences
        thisTime=$(awk "$outputAwkCompare" "$trace"."$exec".out)

        echo "$exec ($thisTime) vs example ($exampleTime): $(./speedup.py $exampleTime $thisTime)"
        echo "$exec ($thisTime) vs noprefetching ($noprefetchingTime): $(./speedup.py $noprefetchingTime $thisTime)"
    done
done

if [ "$1" == "avg" ]; then
    echo -e "\n"
    echo -e "example cycles\t$exampleCycle"
    echo -e "nofetch cycles\t$noPrefetchCycle"
    for exec in ${execs}; do
        thisSpeedup=0
        for trace in $(ls ./traces/*.trace); do
            noPrefetchVal=$(cat $trace.noprefetching.out | awk "$getCycles")
            thisVal=$(cat $trace.$exec.out | awk "$getCycles")
            thisSpeedup=$(./weightedAvg.py $thisSpeedup $noPrefetchVal $thisVal $noPrefetchCycle)
            # echo "$noPrefetchVal/$thisVal = $(./speedup.py $noPrefetchVal $thisVal)"
        done
        thisCycle=$(cat traces/*.trace."$exec".out | awk "$awkCountCycle")
        echo -e "$exec total cycles\t$thisCycle, weighted speedup: $thisSpeedup, unweighted: $(./speedup.py $noPrefetchCycle $thisCycle)"
    done
fi
