#ifndef PREFETCHER_H
#define PREFETCHER_H

#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#include "mem-sim.h"

struct Request;

class Prefetcher {
public:
    Prefetcher(int,char**);
	// should return true if a request is ready for this cycle
	bool hasRequest(uint32_t cycle);

	// request a desired address be brought in
	Request getRequest(uint32_t cycle);

	// this function is called whenever the last prefetcher request was successfully sent to the L2
	void completeRequest(uint32_t cycle);

	/*
	 * This function is called whenever the CPU references memory.
	 * Note that only the addr, pc, load, issuedAt, and HitL1 should be considered valid data
	 */
	void cpuRequest(Request req);

private:
    bool _ready;
	Request _nextReq;
    uint32_t* tribuf;
};

#endif
