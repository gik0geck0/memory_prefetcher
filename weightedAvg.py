#!/usr/bin/env python3

import sys

if len(sys.argv) >= 4:
    cumulativeSpeedup = float(sys.argv[1])
    basetime = float(sys.argv[2])
    opttime = float(sys.argv[3])
    totaltime = float(sys.argv[4])
    cumulativeSpeedup += basetime / totaltime * basetime / opttime
    print("%0.8lf" % (cumulativeSpeedup))
