#include "prefetcher_example.h"
#include "mem-sim.h"

Prefetcher::Prefetcher(int,char**) { }

bool Prefetcher::hasRequest(uint32_t cycle) {
    return false;
}

Request Prefetcher::getRequest(uint32_t cycle) { return Request(); }

void Prefetcher::completeRequest(uint32_t cycle) { }

void Prefetcher::cpuRequest(Request req) { }
